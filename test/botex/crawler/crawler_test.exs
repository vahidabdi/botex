defmodule Botex.CrawlerTest do
  use Botex.DataCase

  alias Botex.Crawler

  describe "news" do
    alias Botex.Crawler.News

    @valid_attrs %{link: "some link"}
    @update_attrs %{link: "some updated link"}
    @invalid_attrs %{link: nil}

    def news_fixture(attrs \\ %{}) do
      {:ok, news} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Crawler.create_news()

      news
    end

    test "list_news/0 returns all news" do
      news = news_fixture()
      assert Crawler.list_news() == [news]
    end

    test "get_news!/1 returns the news with given id" do
      news = news_fixture()
      assert Crawler.get_news!(news.id) == news
    end

    test "create_news/1 with valid data creates a news" do
      assert {:ok, %News{} = news} = Crawler.create_news(@valid_attrs)
      assert news.link == "some link"
    end

    test "create_news/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Crawler.create_news(@invalid_attrs)
    end

    test "update_news/2 with valid data updates the news" do
      news = news_fixture()
      assert {:ok, news} = Crawler.update_news(news, @update_attrs)
      assert %News{} = news
      assert news.link == "some updated link"
    end

    test "update_news/2 with invalid data returns error changeset" do
      news = news_fixture()
      assert {:error, %Ecto.Changeset{}} = Crawler.update_news(news, @invalid_attrs)
      assert news == Crawler.get_news!(news.id)
    end

    test "delete_news/1 deletes the news" do
      news = news_fixture()
      assert {:ok, %News{}} = Crawler.delete_news(news)
      assert_raise Ecto.NoResultsError, fn -> Crawler.get_news!(news.id) end
    end

    test "change_news/1 returns a news changeset" do
      news = news_fixture()
      assert %Ecto.Changeset{} = Crawler.change_news(news)
    end
  end
end
