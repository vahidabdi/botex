defmodule Botex.Fetcher do
  @moduledoc """
  Fetches rss periodically
  """
  use GenServer

  @resources [
    %{
      fetcher: Botex.Crawler.MehrNews,
      sources: [
        %{url: "http://www.mehrnews.com/rss/tp/653", source: "#مهرنیوز", tag: "#بازار"},
        %{url: "http://www.mehrnews.com/rss/tp/25", source: "#مهرنیوز", tag: "#اقتصاد"},
        %{url: "http://www.mehrnews.com/rss-homepage", source: "#مهرنیوز", tag: "#خبر"},
      ],
    },
    %{
      fetcher: Botex.Crawler.Tabnak,
      sources: [
        %{url: "http://www.tabnak.ir/fa/rss/allnews", source: "#تابناک", tag: "#اخبار"},
        %{url: "http://www.tabnak.ir/fa/rss/6", source: "#تابناک", tag: "#اقتصادی"},
        %{url: "http://www.tabnak.ir/fa/rss/3", source: "#تابناک", tag: "#اجتماعی"},
      ]
    },
    %{
      fetcher: Botex.Crawler.YJC,
      sources: [
        %{url: "http://www.yjc.ir/fa/rss/3", source: "#جوان", tag: "#سیاسی"},
        %{url: "http://www.yjc.ir/fa/rss/5", source: "#جوان", tag: "#اجتماعی"},
        %{url: "http://www.yjc.ir/fa/rss/6", source: "#جوان", tag: "#اقتصادی"},
        %{url: "http://www.yjc.ir/fa/rss/3", source: "#جوان", tag: "#سیاسی"},
      ]
    },
    %{
      fetcher: Botex.Crawler.Tabnak,
      sources: [
        %{url: "http://www.farsnews.com/rss/social", source: "#جوان", tag: "#اجتماعی"},
        %{url: "http://www.farsnews.com/rss/economy", source: "#جوان", tag: "#اقتصادی"},
        %{url: "http://www.farsnews.com/rss/world", source: "#جوان", tag: "#جهان"},
        %{url: "http://www.farsnews.com/rss/politics", source: "#جوان", tag: "#سیاسی"},
        %{url: "http://www.farsnews.com/rss/culture", source: "#جوان", tag: "#فرهنگ"},
      ]
    },
    %{
      fetcher: Botex.Crawler.Tabnak,
      sources: [
        %{url: "http://www.varzesh3.com/rss/all", source: "#جوان", tag: "#ورزشی"},
      ]
    },
  ]

  def start_link do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:fetch, state) do
    IO.puts("fetcher " <> inspect self())
    schedule_work()
    {:noreply, state}
  end

  defp schedule_work() do
    Enum.map(@resources, fn resource ->
      #Enum.map(resource.sources, &(resource.fetcher.fetch_and_parse(&1.url, &1.tag)))
      Enum.map(resource.sources, &(apply(resource.fetcher, :fetch_and_parse, [&1.url, &1.tag])))
    end)
    Process.send_after(self(), :fetch, 1 * 60 * 5000)
  end
end
