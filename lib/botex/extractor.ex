defmodule Botex.Extractor do
  use GenServer

  @resources [
    %{
      fetcher: Botex.Extractor.FarsNews,
      sources: [
        %{url: "http://www.farsnews.com/MultiMedia/video", tag: "#ویدیو"},
      ]
    },
    %{
      fetcher: Botex.Extractor.Aparat,
      sources: [
        %{url: "http://www.aparat.com/sport", tag: "#ورزشی"},
        %{url: "http://www.aparat.com/funny", tag: "#طنز"},
        %{url: "http://www.aparat.com/animals", tag: "#حیوانات"},
        %{url: "http://www.aparat.com/entertainment", tag: "#تفریحی"},
        %{url: "http://www.aparat.com/news", tag: "#خبری"},
        %{url: "http://www.aparat.com/accidents", tag: "#حوادث"},
      ]
    },
  ]

  def start_link do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:extract, state) do
    IO.puts("extractor " <> inspect self())
    schedule_work()
    {:noreply, state}
  end

  defp schedule_work do
    Enum.map(@resources, fn resource ->
      Enum.map(resource.sources, &(apply(resource.fetcher, :fetch_and_parse, [&1.url, &1.tag])))
    end)
    Process.send_after(self(), :extract, 1 * 60 * 13000)
  end
end
