defmodule Botex.Crawler.MehrNews do

  @channel "@at_promoter"

  require Logger
  
  def fetch_and_parse(url, tag) do
    with {:ok, %HTTPoison.Response{body: body}} <- HTTPoison.get(url),
         {:ok, feed, _} = FeederEx.parse(body),
         do: check_entries(feed, tag)
  end

  def check_entries(feed, tag) do
    Enum.map(feed.entries, fn item ->
      case Redix.command(:redix, ["GET", item.link]) do
        {:ok, nil} ->
          Redix.command(:redix, ["SETEX", item.link, "864000", "1"])
          send_news(item, tag)
        {:ok, _} -> nil
      end
    end)
  end

  defp send_news(item, tag) do
    case get_in(item, [Access.key(:enclosure, %{}), Access.key(:url, nil)]) do
      nil -> handle_text(item, tag)
      _ -> handle_image(item, tag)
    end
  end

  defp handle_image(item, tag) do
    news_text = news_title(item, tag)
    if String.length(news_text) > 200 do
      handle_text(item, tag)
    else
      with %HTTPoison.Response{body: image} <- HTTPoison.get!(item.enclosure.url),
           do: send_image(item, image, news_text)
    end
  end

  def send_image(item, image, caption) do
    image_file = item.enclosure.url |> String.split("/") |> List.last
    image_path = "/home/vanda/input/" <> image_file
    File.write!(image_path, image)
    System.cmd("/home/vanda/wm.py", [image_path, "/home/vanda/output"])
    Nadia.send_photo(@channel, "/home/vanda/output/" <> image_file, caption: caption)
  end

  defp handle_text(item, tag) do
    news_text = news_title(item, tag)
    Nadia.send_message(@channel, news_text)
  end

  defp news_title(item, tag) do
    Emojix.replace_by_char("✍ " <> item.summary <> "\n\n" <> tag <> jangolak() <> "@ayenehkhabar")
  end

  defp jangolak do
    "\n:small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down:\n"
  end

end
