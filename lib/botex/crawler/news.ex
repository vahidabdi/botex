defmodule Botex.Crawler.News do
  use Ecto.Schema
  import Ecto.Changeset
  alias Botex.Crawler.News


  schema "crawler_news" do
    field :link, :string

    timestamps()
  end

  @doc false
  def changeset(%News{} = news, attrs) do
    news
    |> cast(attrs, [:link])
    |> validate_required([:link])
    |> unique_constraint(:link)
  end
end
