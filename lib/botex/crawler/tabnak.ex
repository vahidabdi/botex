defmodule Botex.Crawler.Tabnak do

  @channel "@at_promoter"

  require Logger

  def fetch_and_parse(url, tag) do
    with {:ok, %HTTPoison.Response{body: body}} <- HTTPoison.get(url),
         {:ok, feed, _} = FeederEx.parse(body),
         do: check_entries(feed, tag)
  end

  def check_entries(feed, tag) do
    Enum.map(feed.entries, fn item ->
      case Redix.command(:redix, ["GET", item.link]) do
        {:ok, nil} ->
          Redix.command(:redix, ["SETEX", item.link, "864000", "1"])
          send_news(item, tag)
        {:ok, _} -> nil
      end
    end)
  end

  defp send_news(item, tag) do
    handle_text(item, tag)
  end

  defp handle_image(item, tag) do
    news_text = news_title(item, tag)
    if String.length(news_text) > 200 do
      handle_text(item, tag)
    else
      with %HTTPoison.Response{body: image} <- HTTPoison.get!(item.enclosure.url),
           do: send_image(item, image, news_text)
    end
  end

  def send_image(item, image, caption) do
    image_file = item.enclosure.url |> String.split("/") |> List.last
    image_path = "/home/vanda/input/" <> image_file
    File.write!(image_path, image)
    System.cmd("/home/vanda/wm.py", [image_path, "/home/vanda/output"])
    Nadia.send_photo(@channel, "/home/vanda/output/" <> image_file, caption: caption)
  end

  defp handle_text(item, tag) do
    news_text = news_title(item, tag)
    Nadia.send_message(@channel, news_text, disable_web_page_preview: true)
  end

  defp news_title(item, tag) do
    summary = Map.get(item, :summary, nil)
    title = Map.get(item, :title, nil)
    if (summary == nil && title == nil) do
      Logger.info("Nothing to send")
    else
      final_title = summary || title
      Emojix.replace_by_char("✍ " <> final_title <> "\n\n" <> tag <> jangolak() <> "@ayenehkhabar\n" <> item.link)
    end
  end

  defp jangolak do
    "\n:small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down:\n"
  end

end
