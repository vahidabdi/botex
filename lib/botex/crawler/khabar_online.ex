defmodule Botex.Crawler.KhabarOnline do

  require Logger

  def fetch_and_parse(url, source, tag) do
    {:ok, %HTTPoison.Response{body: body}} = HTTPoison.get(url)
    {:ok, feed, _} = FeederEx.parse(body)
    Enum.map(feed.entries, fn item ->
      case Redix.command(:redix, ["GET", item.link]) do
        {:ok, nil} ->
          Redix.command(:redix, ["SETEX", item.link, "864000", "1"])
          send_news(item, "@ayenehkhabar", source, tag)
        {:ok, _} -> Logger.error("already in redis")
      end
    end)
  end

  defp send_news(item, channel, source, tag) do
    Logger.info(inspect(item))
    case String.length(item.summary) > 40 do
      true -> handle_text(item, channel, source, tag)
      _ -> Logger.error("needs more description")
    end
  end

  defp handle_text(item, channel, _source, tag) do
    
    first_word = String.split(item.summary, " ") |> hd()
    summary =
      if first_word in ["ایسنا", "ایلنا", "ایرنا", "مهر", "تسنیم", "میزان"] do
        "#" <> item.summary
      else
        item.summary
      end
    news_title = Emojix.replace_by_char("✍ " <> summary <> "\n\n" <> tag <> "\n:earth_africa: " <> channel)
    Nadia.send_message(channel, news_title)
  end
end
