defmodule Botex.Extractor.FarsNews do

  @channel "@at_promoter"
  @site "http://www.farsnews.com/"

  def fetch_and_parse(url, tag) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{body: body}} ->
        urls =
          Floki.find(body, ".latestMedframe .latMednwsTitle a")
          |> Floki.attribute("href")
        Enum.map(urls, fn url ->
          case Redix.command(:redix, ["GET", url]) do
            {:ok, nil} ->
              Redix.command(:redix, ["SETEX", url, "864000", "1"])
              download(@site <> url, tag)
            {:ok, _} -> nil
          end
        end)
      _ ->
        IO.inspect("Nothing")
    end
  end

  defp download(url, tag) do
    with {:ok, %HTTPoison.Response{body: body}} <- HTTPoison.get(url),
         title = Floki.find(body, ".nttitr") |> Floki.text |> String.trim(),
         video_url = Floki.find(body, ".downlink") |> List.last() |> Floki.find("a") |> Floki.attribute("href"),
         :ok <- IO.puts(video_url),
         filename = (Enum.shuffle(?A..?Z) |> List.to_string) <> ".mp4",
         {:ok, %HTTPoison.Response{body: content}} <- HTTPoison.get(video_url),
         output = "/home/vanda/video/" <> filename do
           File.write!(output, content)
           cap = Emojix.replace_by_char("📽 " <> title <> "\n\n" <> tag <> jangolak() <> "@AyenehKhabar")
           Nadia.send_video(@channel, output, caption: cap)
    end
  end

  defp jangolak do
    "\n:small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down:\n"
  end
end
