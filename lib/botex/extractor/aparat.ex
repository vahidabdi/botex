defmodule Botex.Extractor.Aparat do

  @channel "@at_promoter"

  def fetch_and_parse(url, tag) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{body: body}} ->
        vitrin = Floki.find(body, ".block-grid-row.category-vitrin.slider-ab-test")
        urls =
          case vitrin do
            [] ->
              body |>
              Floki.find(".block-grid-column") |> hd() |>
              Floki.find("li.bg-item .video-item__title a") |>
              Floki.attribute("href")
            _ ->
              body |>
              Floki.find(".block-grid-column") |> Enum.at(1) |>
              Floki.find("li.bg-item .video-item__title a") |>
              Floki.attribute("href")
          end
        Enum.map(urls, fn url ->
          case Redix.command(:redix, ["GET", url]) do
            {:ok, nil} ->
              Redix.command(:redix, ["SETEX", url, "864000", "1"])
              download(url, tag)
            {:ok, _} -> nil
          end
        end)
      _ ->
        IO.inspect("Nothing")
    end
  end

  defp download(url, tag) do
    with {:ok, %HTTPoison.Response{body: body}} <- HTTPoison.get(url),
         title = Floki.find(body, ".vone__title") |> Floki.text |> String.trim,
         video_url = Floki.find(body, "[data-ec=download] a") |> Floki.attribute("href") |> hd(),
         {:ok, %HTTPoison.Response{body: content}} <- HTTPoison.get(video_url),
         output = "/home/vanda/video/" <> (video_url |> String.split("/") |> List.last) do
           File.write!(output, content)
           cap = Emojix.replace_by_char("📽 " <> title <> "\n\n" <> tag <> jangolak() <> "@AyenehKhabar")
           Nadia.send_video(@channel, output, caption: cap)
    end
  end

  defp jangolak do
    "\n:small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down::small_red_triangle_down:\n"
  end
end
