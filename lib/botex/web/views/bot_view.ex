defmodule Botex.Web.BotView do
  use Botex.Web, :view

  def render("index.json", %{name: val}) do
    %{"ok": val}
  end
end
