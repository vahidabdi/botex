defmodule Botex.Web.BotController do
  use Botex.Web, :controller

  #@url "https://api.telegram.org/bot"

  def handle_req(conn, %{"bot_token" => _token} = params) do
    IO.inspect(params)
    conn
    |> render("index.json", name: "ok")
  end
end
