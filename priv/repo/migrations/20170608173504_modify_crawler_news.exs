defmodule Botex.Repo.Migrations.ModifyCrawlerNews do
  use Ecto.Migration

  def change do
    alter table(:crawler_news) do
      modify :link, :text
    end
  end
end
