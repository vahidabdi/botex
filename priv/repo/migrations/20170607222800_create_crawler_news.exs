defmodule Botex.Repo.Migrations.CreateBotex.Crawler.News do
  use Ecto.Migration

  def change do
    create table(:crawler_news) do
      add :link, :string

      timestamps()
    end

    create unique_index(:crawler_news, [:link])
  end
end
