# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :botex,
  ecto_repos: [Botex.Repo]

# Configures the endpoint
config :botex, Botex.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "YST+WJyr9DnFQH7gtXCr635HO+z/R/29TRIfX1EqyoST6cHmmKDTiHxI/e4ttw6j",
  render_errors: [view: Botex.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Botex.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# tell logger to load a LoggerFileBackend processes
config :logger,
  backends: [{LoggerFileBackend, :error_log}]

# configuration for the {LoggerFileBackend, :error_log} backend
config :logger, :error_log,
  path: "error.log"
  #level: :error

config :nadia,
  token: "403928621:AAGy7E6vmwn2RkRHGh69rSIO0EKb4lsZPx0"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
